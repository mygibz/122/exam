<#
.SYNOPSIS
Recursively lists all Files and permissions

.DESCRIPTION
The script will recursively output every ile in a directory and output the permissions

.NOTES
Author: Yanik Ammann
Date:  2020-09-21
Scriptname: Berechtigungen auslesen
#>

# Read Parameter (folder)
param (
    [parameter(Mandatory=$true)]
    [String]$rootFolder
)


get-childitem $rootFolder -recurse | get-acl | Out-GridView