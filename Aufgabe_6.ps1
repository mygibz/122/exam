get-WmiObject win32_process -Filter "name='notepad.exe'" | Format-Table ProcessName,
    @{ n= "Total:"; e= {(Get-Date) - $_.ConvertToDateTime($_.CreationDate)}},
    @{ n= "Tiota:"; e= {[Math]::Round($_.VM / 1TB, 2)}},
    @{ n= "ID:"; e= {$_.ProcessID}}