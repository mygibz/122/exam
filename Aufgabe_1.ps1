<#
.SYNOPSIS
Generates 20 grades (between 1 and 6) and outputs statistics about them.

.DESCRIPTION
The script generates 20 grades (between 1 and 6) and will output the highest, the lowest, the average and the sum of the grades.

.NOTES
Author: Yanik Ammann
Date:  2020-09-21
Scriptname: Notensammlung
#>

# Create empty array
$grades = @()

# Adds 20 grades
for ($i = 0; $i -lt 20; $i++) {
    # Generate Grade between 1 and 6
    $grades += Get-Random -Maximum 7 -Minimum 1
}

# output data (alternatively, you could 'assemble' an output using Write-Host)
$grades | Measure-Object -Maximum -Minimum -Average -Sum