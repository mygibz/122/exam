<#
.SYNOPSIS
A script that tests connections to domains and IPs

.DESCRIPTION
This script will ping every domain, given to it via parametes (or default values) and will write the recult in "C:\NetworkMonitor\connectionmonitor.log"

.NOTES
Author: Yanik Ammann
Date:  2020-09-21
Scriptname: Verbindungsmonitor
#>

# Set static Variables
$folderPath = "C:\NetworkMonitor\"
$fileName = "connectionmonitor.log"

# Initiate Array
$domains = @()

# Check if params are provided
if ($args.length -gt 0) {
    # Add each parameter do domain list
    foreach ($parameter in $args) {
        $domains += $parameter
    }
} else {
    # Set default domains
    $domains = "google.ch","1.1.1.1"
}

# Make sure that folder exists, create if it doesnt
if (!(Test-Path $folderPath)) {
    New-Item -ItemType Directory -Path $folderPath | Out-Null
}

# Test connection and write to file
foreach ($domain in $domains) {
    # Get Data
    $date = Get-Date
    $ping = (Test-Connection $domain | measure-Object -Property Latency -Average).average

    # Write to file
    Add-Content -Path "$folderPath\$fileName" -Value "$date  $domain  $ping ms"
}