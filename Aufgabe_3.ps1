<#
.SYNOPSIS
A script, that automatically gets the disk usage and free space from multiple machines

.DESCRIPTION
The script will read the content of 'Computernamen.txt' (filled with hostnames/IP-adresseses) and will (for each one of them) read the disks mounted on that pc (Size, free space, percentage free) and output that data (including the current date) into a CSV file.

.NOTES
Author: Yanik Ammann
Date:  2020-09-21
Scriptname: Hardwareinformationen

PLEASE EXECUTE THIS SCRIPT IN POWERSHELL 5.1!!
#>

# Define static variables
$hostnameFile = "./Computernamen.txt"
$outputFile = "./output_Aufgabe_3.csv"

# Add date to output (also overwrites the 'old' file)
Set-Content -Path $outputFile -Value $(Get-Date)
Add-Content -Path $outputFile -Value "Drive Letter and Name, Drive Size, Free Space, Percentage Free Space"

# Foreach line in file (1 line = 1 PC)
foreach($line in Get-Content $hostnameFile) {
    # go through each disk
    foreach ($disk in $(Get-WmiObject win32_logicaldisk -Computername $line)) {
        # (Annahme das der Nutzer die Datei für Menschen lesbar haben möchte (also mit z.B. GB))

        # Only check disks, that are larger ant 1 byte
        if ($disk.Size -gt 1) {

            # Get Disk Size
            $diskSize = "$([math]::Round($disk.Size / 1GB,2)) GB"
    
            # Get Disk Free
            $diskFree = "$([math]::Round($disk.FreeSpace / 1GB,2)) GB"
    
            # Calculate Disk Free Percentage
            $diskFreePercentage =  "$([math]::Round($disk.FreeSpace * 100 / $disk.Size,2))%"

            # Get Volume Letter (and Name, if available)
            $diskName = $disk.DeviceID + $disk.VolumeName

            # Get Volume Letter (and Name, if available)
            $diskName = $disk.DeviceID + $disk.VolumeName

            # Add Data to file
            Add-Content -Path $outputFile -Value "$diskName,$diskSize,$diskFree,$diskFreePercentage"
        }
    }
}

